                                                                                                                                                                                                
(require 'zone)                                                                                                                                                                                 
                                                                                                                                                                                                
(defun zone-pgm-benjamin ()
  (let* (
         (path "benjamin.txt")
         (txt  (with-temp-buffer
                 (insert-file-contents path)
                 (buffer-string))))
    (setq count 0)
    (erase-buffer)
    (zone-fill-out-screen (window-width) (window-height))
    (goto-char (point-min))
    (while (not (input-pending-p))
      (when (>= count (length txt))
        (goto-char (point-min))
        (erase-buffer)
        (setq count 0))
      (when (eobp)
        (goto-char (point-min))
        (erase-buffer))
      (while (not (eobp))
        (insert (string (elt txt count)))
        (setq count (+ 1 count))
        (zone-park/sit-for (point-min) (* (random 15) 0.01))))))

(eval-after-load "zone"                                                                                                                                                                         
  '(unless (memq 'zone-pgm-benjamin (append zone-programs nil))                                                                                                                                 
     (setq zone-programs                                                                                                                                                                        
           (vconcat zone-programs [zone-pgm-benjamin]))))                                                                                                                                       

;; (defun zone-choose (pgm)
;;     "Choose a PGM to run for `zone'."
;;     (interactive
;;      (list
;;       (completing-read
;;        "Program: "
;;        (mapcar 'symbol-name zone-programs))))
;;     (let ((zone-programs (list (intern pgm))))
;;       (zone)))
